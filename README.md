# shared-ci

## Pre-requisites

- a solution file must exist at the root of the directory (otherwise change the `PROJECT_NAME` variable to point to another file or set it manually)

- `VERSION`, and `PROJECT_NAME` gathering : 

Set `$VERSION` and `$PROJECT_NAME` variables at the very beginning with : 

```yml
default_script:
    - VERSION=`cat VERSION`
    - echo ${VERSION}
    - for f in *.sln; do PROJECT_NAME=${f%.*};done
```

(it will be set for every job that needs it)
